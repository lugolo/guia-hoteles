	$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$(".carousel").carousel({
				interval: 3000
			});

			$("#contacto").on("show.bs.modal", function(e){
				console.log("1. El modal se está mostrando");
				$('#btnContacto').removeClass('btn-outline-success');
				$('#btnContacto').addClass('btn-success');
				$('#btnContacto').prop('disabled', true);

			});

			$("#contacto").on('shown.bs.modal', function(e){
				console.log('2. El modal se ha mostrado');
			});

			$("#contacto").on('hide.bs.modal', function(e){
				console.log('3. Modal Ocultándose HIDE');
			});

			$("#contacto").on('hidden.bs.modal', function(e){
				console.log('4. Modal se ha Ocultado');
				$('#btnContacto').removeClass('btn-success');
				$('#btnContacto').addClass('btn-outline-success');
					$('#btnContacto').prop('disabled', false);
			});
		});